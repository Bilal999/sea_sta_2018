<%@ page import="java.util.*" %>

<%--
	QA To do: Exercise 3 step 8/9
	
	In a JSP scriptlet, declare String variables for fullname, 
	age and email; declare a String array for the interests. 
	Obtain the cookie (as an array of Cookie objects) from the 
	request, and loop through to pick up their values for your 
	variables. The tricky one is the interests, which comes in 
	as a String, but which you want as a String[]. Declare a 
	helper method to do this (see below).
--%>


<DIV align="left">
<H2>Your details: </H2>

<TABLE border="0">

<%--
	QA To do: Exercise 3 step 10

	Via JSP scriptlets, print the fullname, username, age and
	email of the client, using the variables declared earlier,
	instead of the ???s.
--%>

<TR>
<TD width="100" align="right"><B>Name:</B></TD>
<TD align="left">???</TD>
</TR>
<TR>
<TD width="100" align="right"><B>Username:</B></TD>
<TD align="left">???</TD>
</TR>
<TR>
<TD width="100" align="right"><B>Age:</B></TD>
<TD align="left">???</TD>
</TR>
<TR>
<TD width="100" align="right"><B>Email:</B></TD>
<TD align="left">???</TD>
</TR>
<TR>
<TD width="100" align="right" valign="top"><B>Interests:</B></TD>
<TD align="left">
???

<%--
	QA To do: Exercise 3 step 10

	Via JSP scriptlets, using a loop, write out the interests
	as an unnumbered list <UL>...</UL>, with each list item
	<LI>...</LI> being an interest. However, if there are no
	interests, print 'none' in italics.
--%>

</TD>
</TR>
</TABLE>
</DIV>


<%--
		QA To do: Exercise 3 step 9
		
		In a JSP declaration, create a method which splits the 
		cookie�s string, e.g.

			private String[] getBits(String s) {
				StringTokenizer st = new StringTokenizer(s, " ");
				ArrayList res = new ArrayList();
				while (st.hasMoreTokens()) {
					res.add(st.nextToken());
				}
				return (String[]) res.toArray(new String[0]);	
			}
--%>

