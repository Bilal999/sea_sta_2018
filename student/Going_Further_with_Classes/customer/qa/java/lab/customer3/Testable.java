//
//  Testable.java
//

//
// This interface defines five no-argument "test" methods.
// It is designed for use with a test harness that use the
// LiveTable class.
//
package qa.java.lab.customer3;

public interface Testable
{
	void test1();
	void test2();
	void test3();
	void test4();
	void test5();
}
