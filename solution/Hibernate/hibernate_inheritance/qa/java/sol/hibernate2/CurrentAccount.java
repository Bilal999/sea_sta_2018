package qa.java.sol.hibernate2;
import javax.persistence.*;

@Entity
@DiscriminatorValue("CA")
public class CurrentAccount extends BankAccount implements java.io.Serializable {

	@Column(name="overdraft_limit")
	private double overdraftLimit;

	public double getOverdraftLimit() {
		return overdraftLimit;
	}

	public void setOverdraftLimit(double overdraftLimit) {
		this.overdraftLimit = overdraftLimit;
	}
	
	//business method: override the method that checks whether there's enough money 
	//to support the requested withdrawal amount - taking the OVERDRAFT in account here 
	
	protected boolean sufficentFundsAvailable(double amount){
		return (amount <= super.getBalance() + overdraftLimit);
	}
	
}
