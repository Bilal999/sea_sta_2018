package qa.java.sol.shapes2;

import java.awt.*;

public interface Drawable
{
	public abstract void draw(Graphics g);
}