/* Sort is a class for sorting integer numbers */
package qa.java.sol.sort;

public class Sort
{
	// The bSort() class method implements a simple 'bubble sort' algorithm to
	// sort an array of integers into ascending order
	//
	public static void bSort(int[] nums)
	{
		for (int i = 0; i < nums.length - 1; i++)
		{
			for (int j = i + 1; j < nums.length; j++)
			{
				if (nums[i] > nums[j])
				{
					// swap order
					int temp = nums[j];
					nums[j] = nums[i];
					nums[i] = temp;
				}
			}
		}
	}


}




