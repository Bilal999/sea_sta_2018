<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<%@ page import='java.sql.*,javax.sql.*,javax.naming.*' %>
<TITLE>populate.jsp</TITLE>
</HEAD>
<BODY>
<H1>Populate the Database Cheap and Easy!</H1>

<%
	DataSource ds = null;
	try {
		Context ic = new InitialContext();
		out.println("<br><font color='blue'>JNDI lookup for 'jdbc/festival'.</font>");
		ds = (DataSource) ic.lookup("java:comp/env/jdbc/festival");
	} catch (Exception e) {
		out.println("<br><font color='red'>JNDI lookup failed: " + e.getMessage() + "</font>");
		e.printStackTrace();
	}
	Connection con = null;
	try {
		con = ds.getConnection();
		for (int i=0; i<setup.length; i++) {
			try {
				Statement stmt = con.createStatement();
				stmt.executeUpdate(setup[i]);
				out.println("<br>Success: " + setup[i]);
			} catch (Exception e) {
				out.println("<br><font color='green'>Ignoring: " + e.getMessage() + "</font>");
				e.printStackTrace();
			}
		}

		for (int i=0; i<inserts.length; i++) {
			try {
				Statement stmt = con.createStatement();
				stmt.executeUpdate(inserts[i]);
				out.println("<br>Success: " + inserts[i]);
			} catch (Exception e) {
				out.println("<br><font color='red'>Fatal insert: " + e.getMessage() + "</font>");
				e.printStackTrace();
			}
		}

	} catch (Exception e) {
		out.println("<br><font color='red'>JDBC?: " + e.getMessage() + "</font>");
		e.printStackTrace();
	} finally {
		try {
			out.println("<br><font color='blue'>Closing connection.</font>");
			con.close();
		} catch (Exception e) {
			out.println("<br><font color='red'>Close failed: " + e.getMessage() + "</font>");
			e.printStackTrace();
		}
	}
%>

<%!
	String[] setup = {"DROP TABLE DBO.plays", 
				"DROP TABLE DBO.films", 
				"DROP TABLE DBO.operas", 
				"DROP TABLE DBO.events", 
				"DROP SCHEMA DBO", 
				"CREATE SCHEMA DBO",
		"CREATE TABLE DBO.plays (eventid int NOT NULL, play VARCHAR(255), author VARCHAR(50), starttime VARCHAR(20), festivalday int NOT NULL )",
		"CREATE TABLE DBO.operas (id int NOT NULL, title VARCHAR(255), composer VARCHAR(255), starttime VARCHAR(20), festivalday int NOT NULL )",
		"CREATE TABLE DBO.films (filmID int NOT NULL, title varchar (255) NOT NULL, director varchar (255) NOT NULL, actor1 varchar (255) NOT NULL, actor2 varchar (255) NOT NULL, actor3 varchar (255) NOT NULL, length int NOT NULL, starttime VARCHAR(20), festivalday int NOT NULL )",
		"CREATE TABLE DBO.events (eventid int primary key NOT NULL, seats int NOT NULL, bookingID int NOT NULL)"};
	
	String[] inserts = {
		"INSERT INTO DBO.plays VALUES (1201,'Hamlet','Shakespeare','7:00pm',1)",
		"INSERT INTO DBO.plays VALUES (1202,'Pygmallion','Bernard Shaw','8:00pm',2)",
		"INSERT INTO DBO.plays VALUES (1203,'Salome','Wilde','7:30pm',3)",
		"INSERT INTO DBO.plays VALUES (1204,'Amadeus','Schaffer','7:30pm',4)",
		"INSERT INTO DBO.plays VALUES (1205,'Waiting for Godot','Beckett','8:00pm',5)",
		"INSERT INTO DBO.plays VALUES (1206,'Death of a Salesman','Miller','8:00pm',6)",
		"INSERT INTO DBO.plays VALUES (1207,'Much Ado About Nothing','Shakespeare','7:00pm',7)",
		"INSERT INTO DBO.plays VALUES (1208,'Doctor Faustus','Marlowe','8:00pm',8)",
		"INSERT INTO DBO.plays VALUES (1209,'Richard III','Shakespeare','7:00pm',9)",
		"INSERT INTO DBO.plays VALUES (1210,'Woyzeck','Buchner','8:00pm',10)",
		"INSERT INTO DBO.plays VALUES (1211,'Brimstone and Treacle','Potter','7:30pm',11)",
		"INSERT INTO DBO.plays VALUES (1212,'Democracy','Frayn','7:30pm',12)",
		"INSERT INTO DBO.plays VALUES (1213,'The Frogs','Aristophanes','8:00pm',13)",
		"INSERT INTO DBO.plays VALUES (1214,'Oedipus Rex','Sophocles','8:00pm',14)",

		"INSERT INTO DBO.films VALUES (34001,'Blade Runner','Ridley Scott','Harrison Ford','Rutger Hauer','Sean Young',123,'8pm',1)",
		"INSERT INTO DBO.films VALUES (34002,'The Day the Earth Stood Still','Robert Wise','Michael Rennie','Patricia Neal','Lock Martin',115,'7pm',2)",
		"INSERT INTO DBO.films VALUES (34003,'The Thing','John Carpenter','Kurt Russell','Wilfred Brimley','Richard Dysart',87,'7.15pm',3)",
		"INSERT INTO DBO.films VALUES (34004,'The Fifth Element','Luc Besson','Bruce Willis','Milla Jovovich','Gary Oldman',94,'8pm',4)",
		"INSERT INTO DBO.films VALUES (34005,'The Forbin Project','Joseph Sargent','Eric Braeden','Susan Clark','James Winters',102,'8pm',5)",
		"INSERT INTO DBO.films VALUES (34006,'Invasion of the Body Snatchers','Don Siegel','Kevin McCarthy','Dana Wynter','Sam Peckinpah',122,'7.30pm',6)",
		"INSERT INTO DBO.films VALUES (34007,'Logans Run','Michael Anderson','Michael York','Richard Jordan','Jenny Agutter',135,'7.30pm',7)",
		"INSERT INTO DBO.films VALUES (34008,'Mars Attacks!','Tim Burton','Michael J Fox','Pierce Brosnan','Jack Nicholson',97,'8pm',8)",
		"INSERT INTO DBO.films VALUES (34009,'Metropolis','Fritz Lang','Alfred Abel','Gustav Frohlich','Herbert Wells',99,'9pm',9)",
		"INSERT INTO DBO.films VALUES (34010,'The Omega Man','Boris Sagal','Charlton Heston','Anthony Zerbe','Fred B',89,'7.30pm',10)",
		"INSERT INTO DBO.films VALUES (34011,'Panic in the Year Zero','Ray Milland','Ray Milland','Jean Hagen','Frankie Avalon',111,'7.45pm',11)",
		"INSERT INTO DBO.films VALUES (34012,'Plan 9 from Outer Space','Ed Wood','Gregory Walcott','Bela Lugosi','Lewis',100,'8pm',12)",
		"INSERT INTO DBO.films VALUES (34013,'Edward Scissorhands','Tim Burton','Johnny Depp','Winona Ryder','Dianne West',93,'8pm',13)",
		"INSERT INTO DBO.films VALUES (34014,'The Empire Strikes Back','Irvin Kershner','Mark Hamill','Harrison Ford','Carrie Fisher',132,'7.30pm',14)",

		"INSERT INTO DBO.operas VALUES (201,'Elektra','Strauss','7:30pm',1)",
		"INSERT INTO DBO.operas VALUES (202,'Lulu','Berg','7:30pm',2)",
		"INSERT INTO DBO.operas VALUES (203,'Iphigenie en Tauride','Gluck','7:30pm',3)",
		"INSERT INTO DBO.operas VALUES (204,'Madam Butterfly','Puccini','7:30pm',4)",
		"INSERT INTO DBO.operas VALUES (205,'Lucia di Lammermoor','Donizetti','7:30pm',5)",
		"INSERT INTO DBO.operas VALUES (206,'Rigoletto','Verdi','7:30pm',6)",
		"INSERT INTO DBO.operas VALUES (207,'Lady Macbeth of Mtsensk','Shostakovich','7:30pm',7)",
		"INSERT INTO DBO.operas VALUES (208,'Katya Kabanova','Janacek','7:30pm',8)",
		"INSERT INTO DBO.operas VALUES (209,'Tristan und Isolde','Wagner','7:30pm',9)",
		"INSERT INTO DBO.operas VALUES (210,'Orfeo','Monteverdi','7:30pm',10)",
		"INSERT INTO DBO.operas VALUES (211,'Manon','Massenet','7:30pm',11)",
		"INSERT INTO DBO.operas VALUES (212,'Semiramide','Rossini','7:30pm',12)",
		"INSERT INTO DBO.operas VALUES (213,'Norma','Bellini','7:30pm',13)",
		"INSERT INTO DBO.operas VALUES (214,'Dido and Aeneas','Purcell','7:30pm',14)",

		"INSERT INTO DBO.events VALUES (1201,0,61)",
		"INSERT INTO DBO.events VALUES (1202,11,86)",
		"INSERT INTO DBO.events VALUES (1203,22,23)",
		"INSERT INTO DBO.events VALUES (1204,0,80)",
		"INSERT INTO DBO.events VALUES (1205,14,41)",
		"INSERT INTO DBO.events VALUES (1206,3,21)",
		"INSERT INTO DBO.events VALUES (1207,0,35)",
		"INSERT INTO DBO.events VALUES (1208,16,44)",
		"INSERT INTO DBO.events VALUES (1209,0,88)",
		"INSERT INTO DBO.events VALUES (1210,34,32)",
		"INSERT INTO DBO.events VALUES (1211,45,22)",
		"INSERT INTO DBO.events VALUES (1212,8,77)",
		"INSERT INTO DBO.events VALUES (1213,1,65)",
		"INSERT INTO DBO.events VALUES (1214,0,72)",

		"INSERT INTO DBO.events VALUES (34001,15,97)",
		"INSERT INTO DBO.events VALUES (34002,8,110)",
		"INSERT INTO DBO.events VALUES (34003,43,45)",
		"INSERT INTO DBO.events VALUES (34004,10,105)",
		"INSERT INTO DBO.events VALUES (34005,0,132)",
		"INSERT INTO DBO.events VALUES (34006,0,117)",
		"INSERT INTO DBO.events VALUES (34007,14,82)",
		"INSERT INTO DBO.events VALUES (34008,34,43)",
		"INSERT INTO DBO.events VALUES (34009,23,55)",
		"INSERT INTO DBO.events VALUES (34010,15,56)",
		"INSERT INTO DBO.events VALUES (34011,1,111)",
		"INSERT INTO DBO.events VALUES (34012,3,105)",
		"INSERT INTO DBO.events VALUES (34013,1,132)",
		"INSERT INTO DBO.events VALUES (34014,12,87)",

		"INSERT INTO DBO.events VALUES (201,2,89)",
		"INSERT INTO DBO.events VALUES (202,15,57)",
		"INSERT INTO DBO.events VALUES (203,21,35)",
		"INSERT INTO DBO.events VALUES (204,0,90)",
		"INSERT INTO DBO.events VALUES (205,1,97)",
		"INSERT INTO DBO.events VALUES (206,0,96)",
		"INSERT INTO DBO.events VALUES (207,12,68)",
		"INSERT INTO DBO.events VALUES (208,3,88)",
		"INSERT INTO DBO.events VALUES (209,0,100)",
		"INSERT INTO DBO.events VALUES (210,4,89)",
		"INSERT INTO DBO.events VALUES (211,17,56)",
		"INSERT INTO DBO.events VALUES (212,21,45)",
		"INSERT INTO DBO.events VALUES (213,10,77)",
		"INSERT INTO DBO.events VALUES (214,1,93)"};
%>

</BODY>
</HTML>
