
$(document).ready(function(){

	$("#menu > li > ul").hide().click(function(event){
		event.stopPropagation();
	});


	//hide the inner menus
	//$("#menu > li > ul").hide();

	//the menu items expand into menus of their own...
	$("#menu li").mouseover(function(){
		$(this).find("ul").slideDown();
	});
	$("#menu li").mouseout(function(){
		$(this).find("ul").slideUp();
	});


	//handle tabs
	//initialise...set 1st tab to selected 
	$("#tab_list li:first").addClass("selected");

	//initialise...hide all content except 1st para
	$("#content_div p:not(:first)").hide();
	
	//enable click-event handler for all tabs
	$("#tab_list li").click(function(){
		//deal with the tabs
		//first set all tabs to not selected...
		$("#tab_list li").removeClass("selected");
		//now set the one that was clicked to selected...
		$(this).addClass("selected");
		//tabs are done... now deal with the content
		//first hide all content paras
		$("#content_div p").hide();
		//now fade in the clicked para...
		var selectlink = $(this).find("a:first").attr("href");
		$(selectlink).fadeIn("fast");
	});
	
	
	
	//lighten the background colour of alternating columns in table
	//(this technique workds only if the number of columns is even)
	$("tr td:even").css("background-color", "#117711");
	//change the background colour of a row while it's being pointed at
	//note that the hover function combines mouseover and mouseout: it
	//therefore requires two functions as arguments, one for pointer moving onto
	//the element and one for leaving the element
	$("tbody tr").hover(function(){
		$(this).css("background-color", "#E05500");
	}, function(){
		$(this).css("background-color", "#004422");
	});

 	
});